# Contributing


## Code of Conduct

Please see [CODE_OF_CONDUCT.md](./CODE_OF_CONDUCT.md)


## Contributor License Agreement (CLA)

Before accepting your contributions to this project,
we require you to sign one of the below CLAs as appropriate
(these are external links to DocuSign):

-   [Corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)

-   [Individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


## Development

-   install dependencies with `make setup`

-   run tests with `make test`

-   run tests and check for race conditions with `make test-race`

-   run benchmarks with `make test-bench`

-   check coverage with `make test-cover`
