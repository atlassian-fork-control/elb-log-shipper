GIT_HASH := $$(git rev-parse --short=10 HEAD)

BINARY_NAME := elb-log-shipper
DOCKER_TAG := "$(BINARY_NAME):$(GIT_HASH)"

ARCH ?= darwin
GOVERSION := 1.9
GP := /gopath
MAIN_PKG := bitbucket.org/atlassian/elb-log-shipper/cmd/elb-log-shipper
DOCKER_GO_FLAGS := --rm \
	-v "$(GOPATH)":"$(GP)" \
	-w "$(GP)/src/bitbucket.org/atlassian/elb-log-shipper" \
	-e GOPATH="$(GP)"

setup:
	go get -u github.com/golang/dep/cmd/dep
	dep ensure
	go get -u golang.org/x/tools/cmd/goimports

build: fmt setup
	mkdir -p build/bin/$(ARCH)
	go build -o build/bin/$(ARCH)/$(BINARY_NAME) $(MAIN_PKG)

build-race: fmt setup
	mkdir -p build/bin/$(ARCH)
	go build -race -o build/bin/$(ARCH)/$(BINARY_NAME) $(MAIN_PKG)

build-all: fmt setup
	go build ./...

fmt: setup
	gofmt -w=true -s $$(find . -type f -name '*.go' -not -path "./vendor/*")
	goimports -w=true -d $$(find . -type f -name '*.go' -not -path "./vendor/*")

test: setup
	go test ./...

test-bench: setup
	go test -bench . -benchmem -benchtime 5s ./...

test-cover: setup
	go test -cover ./...

test-race: setup
	go test -race ./...

ci: setup build build-race test test-race test-cover

# Compile a static binary. Cannot be used with -race
docker-go-build:
	docker pull golang:$(GOVERSION)
	mkdir -p build/bin/linux
	docker run $(DOCKER_GO_FLAGS) -e CGO_ENABLED=0 golang:$(GOVERSION) go build -o build/bin/linux/$(BINARY_NAME) $(MAIN_PKG)

docker-build: docker-go-build
	docker build --pull -t $(DOCKER_TAG) build

# Compile a binary which checks for data races.
docker-go-build-race:
	docker pull golang:$(GOVERSION)
	mkdir -p build/bin/linux
	docker run $(DOCKER_GO_FLAGS) golang:$(GOVERSION) go build -race -o build/bin/linux/$(BINARY_NAME) $(MAIN_PKG)

docker-tag-hash:
	echo "docker_tag_hash=$(GIT_HASH)" > docker-tag-hash

clean:
	cd pkg/logserver/; rm *.pb.go
	cd bin/; go clean

.PHONY: setup build build-race build-all fmt test test-cover test-race ci docker-go-build docker-build docker-go-build-race docker-tag-hash clean
