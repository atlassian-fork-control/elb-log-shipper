package logserver

import (
	"bufio"
	"context"
	"io"
	"sync"

	"github.com/DataDog/datadog-go/statsd"
	log "github.com/Sirupsen/logrus"
)

const (
	omittedLogLinesMetric  = "omitted_log_lines"
	parseResultBufferLimit = 100
)

type LoadBalancerLogParser struct {
	Statsd     *statsd.Client
	LineParser LogLineParser
	Workers    int

	// a "logger" would have been preferred,
	// but logrus does not expose its default global singleton
	logWithFields func(log.Fields) *log.Entry
}

func NewLoadBalancerLogParser() *LoadBalancerLogParser {
	return &LoadBalancerLogParser{
		logWithFields: log.WithFields,
	}
}

// ParseReader returns a channel of per-result channels
func (p *LoadBalancerLogParser) ParseReader(ctx context.Context, reader io.Reader, logFileInfo *LogFileInfo) <-chan <-chan ParseResult {
	parsedEntries := make(chan (<-chan ParseResult), parseResultBufferLimit)

	type inputLine struct {
		text string
		sink chan<- ParseResult
		row  int
	}
	logLines := make(chan inputLine)
	wg := sync.WaitGroup{}
	wg.Add(p.Workers)

	// Any error encountered while reading the logfile.
	var readError error

	go func() {
		defer close(logLines)
		scanner := bufio.NewScanner(reader)
		row := 0
		for scanner.Scan() {
			sink := make(chan ParseResult, 1)
			row++
			line := inputLine{
				text: scanner.Text(),
				sink: sink,
				row:  row,
			}
			select {
			case <-ctx.Done():
				readError = ctx.Err()
				return
			case logLines <- line:
				select {
				case <-ctx.Done():
					readError = ctx.Err()
					return
				case parsedEntries <- sink:
				}
			}
		}
		readError = scanner.Err()
	}()

	for i := 0; i < p.Workers; i++ {
		// Worker which parses logLines read from the input file.
		go func() {
			defer wg.Done()
			for line := range logLines {
				// If there's an error while parsing the line, no amount of retries will fix this. We should log the
				// error and the line and carry on.
				logLineEntry, err := p.LineParser.ParseLine(line.text)
				if err != nil {
					p.logWithFields(log.Fields{
						"file": logFileInfo.LogFilename,
						"text": line.text,
						"row":  line.row,
					}).Warningf("omitting log line: %v", err)
					p.Statsd.Incr(omittedLogLinesMetric, nil, 1)
					close(line.sink)
					continue
				}
				line.sink <- ParseResult{logLineEntry, nil}
			}
		}()
	}

	// Cleanup function: closes the channel after we've parsed all the log lines.
	go func() {
		wg.Wait()
		if readError != nil {
			resultChan := make(chan ParseResult, 1)
			resultChan <- ParseResult{nil, readError}
			select {
			case parsedEntries <- resultChan:
			case <-ctx.Done():
			}
		}
		close(parsedEntries)
	}()
	return parsedEntries
}
