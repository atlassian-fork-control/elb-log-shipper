package logserver

import (
	"context"
	"errors"
	"io"
	"strings"
	"sync/atomic"
	"testing"

	log "github.com/Sirupsen/logrus"
	logtest "github.com/Sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type mockLineParser struct {
	t                *testing.T
	responses        map[string]ParseResult
	numCalls         uint64
	numExpectedCalls uint64
}

func (p *mockLineParser) ParseLine(line string) (*LogLineEntry, error) {
	// Increment before check to avoid race condition where both checks before Increments.
	atomic.AddUint64(&p.numCalls, 1)
	assert.True(p.t, atomic.LoadUint64(&p.numCalls) <= p.numExpectedCalls, "too many calls to ParseLine")
	assert.Contains(p.t, p.responses, line)
	return p.responses[line].logLineEntry, p.responses[line].err
}

type mockErrorReader struct {
	err error
}

func (r *mockErrorReader) Read(p []byte) (int, error) {
	return 0, r.err
}

func TestParseReaderParsing(t *testing.T) {
	t.Parallel()

	type parseReaderTest struct {
		name            string
		lineParser      *mockLineParser
		reader          io.Reader
		expectedOutput  []ParseResult
		expectedLogging []*log.Entry
	}
	parseReaderTests := []parseReaderTest{
		{
			"Omits error lines",
			&mockLineParser{
				responses: map[string]ParseResult{
					"first": {
						logLineEntry: &LogLineEntry{ConnectionType: "a"},
						err:          nil,
					},
					"second": {
						logLineEntry: nil,
						err:          errors.New("invalid line"),
					},
					"third": {
						logLineEntry: &LogLineEntry{ConnectionType: "c"},
						err:          nil,
					},
				},
				numExpectedCalls: 3,
			},
			strings.NewReader("first\nsecond\nthird"),
			[]ParseResult{
				{
					logLineEntry: &LogLineEntry{ConnectionType: "a"},
					err:          nil,
				},
				{
					logLineEntry: nil,
					err:          nil,
				},
				{
					logLineEntry: &LogLineEntry{ConnectionType: "c"},
					err:          nil,
				},
			},
			[]*log.Entry{
				{
					Data: log.Fields{
						"file": "parse-test.log",
						"row":  2,
						"text": "second",
					},
					Level:   log.WarnLevel,
					Message: "omitting log line: invalid line",
				},
			},
		},
		{
			"Handles reader errors",
			&mockLineParser{},
			&mockErrorReader{err: errors.New("reader error")},
			[]ParseResult{
				{
					err: errors.New("some error"),
				},
			},
			[]*log.Entry{},
		},
	}

	for _, test := range parseReaderTests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.lineParser.t = t
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			logger, hook := logtest.NewNullLogger()

			parser := NewLoadBalancerLogParser()
			parser.LineParser = test.lineParser
			parser.Workers = 2
			parser.logWithFields = logger.WithFields

			parseResults := parser.ParseReader(ctx, test.reader, &LogFileInfo{LogFilename: "parse-test.log"})
			resultsReceived := 0
			for resultChan := range parseResults {
				require.True(t, resultsReceived < len(test.expectedOutput), "received too many parse results")
				result, ok := <-resultChan
				expectedResult := test.expectedOutput[resultsReceived]
				if expectedResult.logLineEntry == nil && expectedResult.err == nil {
					assert.False(t, ok)
				} else {
					if expectedResult.err != nil { // expectedResult.logLineEntry == nil
						assert.Error(t, result.err)
					} else { // expectedResult.logLineEntry != nil
						require.NoError(t, result.err)
						assert.Equal(t, expectedResult, result)
					}
				}
				resultsReceived++
			}
			assert.Equal(t, len(test.expectedOutput), resultsReceived)

			// checking `len()` first to avoid nil slice gotcha
			assert.Equal(t, len(test.expectedLogging), len(hook.Entries))
			for i, expectedLog := range test.expectedLogging {
				assert.Equal(t, expectedLog.Data, hook.Entries[i].Data)
				assert.Equal(t, expectedLog.Level, hook.Entries[i].Level)
				assert.Equal(t, expectedLog.Message, hook.Entries[i].Message)
			}
		})
	}
}
