package logserver

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/elb"
	"github.com/aws/aws-sdk-go/service/elbv2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type mockElbService struct {
	t        *testing.T
	elbName  string
	output   *elb.DescribeTagsOutput
	err      error
	numCalls int
}

func (s *mockElbService) DescribeTagsElb(ctx context.Context, input *elb.DescribeTagsInput) (*elb.DescribeTagsOutput, error) {
	require.NotNil(s.t, input)
	require.Len(s.t, input.LoadBalancerNames, 1)
	require.Equal(s.t, s.elbName, *input.LoadBalancerNames[0])
	s.numCalls++
	return s.output, s.err
}

type mockElbv2Service struct {
	t        *testing.T
	albArn   string
	output   *elbv2.DescribeTagsOutput
	err      error
	numCalls int
}

func (s *mockElbv2Service) DescribeTagsElbv2(ctx context.Context, input *elbv2.DescribeTagsInput) (*elbv2.DescribeTagsOutput, error) {
	require.NotNil(s.t, input)
	require.Len(s.t, input.ResourceArns, 1)
	require.Equal(s.t, s.albArn, *input.ResourceArns[0])
	s.numCalls++
	return s.output, s.err
}

type stubTagCacher struct {
}

func (c *stubTagCacher) GetTags(name string) (map[string]string, bool) {
	return nil, false
}

func (c *stubTagCacher) PutTags(name string, tags map[string]string) {
}

func TestFetchTagsElb(t *testing.T) {
	t.Parallel()
	fetchTests := []struct {
		name      string
		service   *mockElbService
		elbName   string
		expected  map[string]string
		expectErr bool
	}{
		{
			"Omit tags from other ELBs",
			&mockElbService{
				elbName: "elb1",
				output: &elb.DescribeTagsOutput{
					TagDescriptions: []*elb.TagDescription{
						{
							LoadBalancerName: aws.String("elb1"),
							Tags: []*elb.Tag{
								{Key: aws.String("fruit"), Value: aws.String("apple")},
							},
						},
						{
							LoadBalancerName: aws.String("elb2"),
							Tags: []*elb.Tag{
								{Key: aws.String("feeling"), Value: aws.String("happy")},
							},
						},
					},
				},
				err: nil,
			},
			"elb1",
			map[string]string{
				"fruit": "apple",
			},
			false,
		},
		{
			"Handle error describing ELB",
			&mockElbService{
				elbName: "elbname",
				output:  nil,
				err:     errors.New("test error"),
			},
			"elbname",
			nil,
			true,
		},
	}

	for _, test := range fetchTests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.service.t = t

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			actual, err := (&LoadBalancerTagFetcher{
				ElbService: test.service,
				Cacher:     &stubTagCacher{},
			}).FetchTagsElb(ctx, test.elbName)
			if test.expectErr {
				assert.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, test.expected, actual)
			}
		})
	}
}

func TestFetchTagsElbCaches(t *testing.T) {
	t.Parallel()
	elbService := &mockElbService{
		t:       t,
		elbName: "elb",
		output: &elb.DescribeTagsOutput{
			TagDescriptions: []*elb.TagDescription{
				{
					LoadBalancerName: aws.String("elb"),
					Tags: []*elb.Tag{
						{Key: aws.String("fruit"), Value: aws.String("apple")},
					},
				},
			},
		},
		err: nil,
	}

	cacher := NewLoadBalancerTagCacher()
	cacher.CleanupInterval = time.Minute
	cacher.ExpirationTime = time.Minute

	fetcher := &LoadBalancerTagFetcher{
		ElbService: elbService,
		Cacher:     cacher,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for i := 0; i < 2; i++ {
		tags, err := fetcher.FetchTagsElb(ctx, "elb")
		require.NoError(t, err)
		require.Equal(t, map[string]string{
			"fruit": "apple",
		}, tags)
	}
	assert.Equal(t, 1, elbService.numCalls)
}

func TestFetchTagsElbCachesErrorAsNil(t *testing.T) {
	t.Parallel()
	elbService := &mockElbService{
		t:       t,
		elbName: "elb",
		output:  nil,
		err:     errors.New("LoadBalancerNotFound"),
	}

	cacher := NewLoadBalancerTagCacher()
	cacher.CleanupInterval = time.Minute
	cacher.ExpirationTime = time.Minute

	fetcher := &LoadBalancerTagFetcher{
		ElbService: elbService,
		Cacher:     cacher,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	_, err := fetcher.FetchTagsElb(ctx, "elb")
	require.Error(t, err)

	var tags map[string]string
	tags, err = fetcher.FetchTagsElb(ctx, "elb")
	require.NoError(t, err)
	assert.Nil(t, tags)
	assert.Equal(t, 1, elbService.numCalls)
}

func TestFetchTagsElbv2(t *testing.T) {
	t.Parallel()
	fetchTests := []struct {
		name      string
		service   *mockElbv2Service
		region    string
		accountId string
		albName   string
		expected  map[string]string
		expectErr bool
	}{
		{
			"Omit tags from other ALBs",
			&mockElbv2Service{
				albArn: "arn:aws:elasticloadbalancing:myregion:myaccountid:loadbalancer/alb2",
				output: &elbv2.DescribeTagsOutput{
					TagDescriptions: []*elbv2.TagDescription{
						{
							ResourceArn: aws.String("arn:aws:elasticloadbalancing:myregion:myaccountid:loadbalancer/alb1"),
							Tags: []*elbv2.Tag{
								{Key: aws.String("fruit"), Value: aws.String("apple")},
							},
						},
						{
							ResourceArn: aws.String("arn:aws:elasticloadbalancing:myregion:myaccountid:loadbalancer/alb2"),
							Tags: []*elbv2.Tag{
								{Key: aws.String("feeling"), Value: aws.String("happy")},
							},
						},
					},
				},
				err: nil,
			},
			"myregion",
			"myaccountid",
			"alb2",
			map[string]string{
				"feeling": "happy",
			},
			false,
		},
		{
			"Handle error describing ALB",
			&mockElbv2Service{
				albArn: "arn:aws:elasticloadbalancing:someregion:someaccountid:loadbalancer/albname",
				output: nil,
				err:    errors.New("test error"),
			},
			"someregion",
			"someaccountid",
			"albname",
			nil,
			true,
		},
	}

	for _, test := range fetchTests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.service.t = t

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			actual, err := (&LoadBalancerTagFetcher{
				Elbv2Service: test.service,
				Cacher:       &stubTagCacher{},
			}).FetchTagsElbv2(ctx, test.region, test.accountId, test.albName)
			if test.expectErr {
				assert.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, test.expected, actual)
			}
		})
	}
}

func TestFetchTagsElbv2Caches(t *testing.T) {
	t.Parallel()
	elbv2Service := &mockElbv2Service{
		t:      t,
		albArn: "arn:aws:elasticloadbalancing:someregion:someaccountid:loadbalancer/albname",
		output: &elbv2.DescribeTagsOutput{
			TagDescriptions: []*elbv2.TagDescription{
				{
					ResourceArn: aws.String("arn:aws:elasticloadbalancing:someregion:someaccountid:loadbalancer/albname"),
					Tags: []*elbv2.Tag{
						{Key: aws.String("fruit"), Value: aws.String("apple")},
					},
				},
			},
		},
		err: nil,
	}

	cacher := NewLoadBalancerTagCacher()
	cacher.CleanupInterval = time.Minute
	cacher.ExpirationTime = time.Minute

	fetcher := &LoadBalancerTagFetcher{
		Elbv2Service: elbv2Service,
		Cacher:       cacher,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for i := 0; i < 2; i++ {
		tags, err := fetcher.FetchTagsElbv2(ctx, "someregion", "someaccountid", "albname")
		require.NoError(t, err)
		assert.Equal(t, map[string]string{
			"fruit": "apple",
		}, tags)
	}
	assert.Equal(t, 1, elbv2Service.numCalls)
}

func TestFetchTagsElbv2CachesErrorAsNil(t *testing.T) {
	t.Parallel()
	elbv2Service := &mockElbv2Service{
		t:      t,
		albArn: "arn:aws:elasticloadbalancing:someregion:someaccountid:loadbalancer/albname",
		output: nil,
		err:    errors.New("LoadBalancerNotFound"),
	}

	cacher := NewLoadBalancerTagCacher()
	cacher.CleanupInterval = time.Minute
	cacher.ExpirationTime = time.Minute

	fetcher := &LoadBalancerTagFetcher{
		Elbv2Service: elbv2Service,
		Cacher:       cacher,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	_, err := fetcher.FetchTagsElbv2(ctx, "someregion", "someaccountid", "albname")
	require.Error(t, err)

	var tags map[string]string
	tags, err = fetcher.FetchTagsElbv2(ctx, "someregion", "someaccountid", "albname")
	require.NoError(t, err)
	assert.Nil(t, tags)
	assert.Equal(t, 1, elbv2Service.numCalls)
}
