package logserver

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type LogOpener struct {
	SqsService    SQS
	Openers       []S3LogFileOpener
	QueueUrl      string
	ReceiptHandle string
}

func (l *LogOpener) Success(ctx context.Context) error {
	deleteMessageParams := &sqs.DeleteMessageInput{
		QueueUrl:      aws.String(l.QueueUrl),
		ReceiptHandle: aws.String(l.ReceiptHandle),
	}
	if _, err := l.SqsService.DeleteMessage(ctx, deleteMessageParams); err != nil {
		return ContextOrError(ctx, "error deleting message with handle %s: %v", l.ReceiptHandle, err)
	}
	return nil
}
