package logserver

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"bitbucket.org/atlassian/elb-log-shipper/pkg/redact"

	"github.com/DataDog/datadog-go/statsd"
	log "github.com/Sirupsen/logrus"
)

const (
	messageProcessingFailedMetric = "message_processing_failures"
	messageProcessingTimeMetric   = "message_processing_time"
	logfileProcessingTimeMetric   = "logfile_processing_time"
	logfileSizeMetric             = "logfile_size"
)

type ParseResult struct {
	logLineEntry *LogLineEntry
	err          error
}

type LogEntryWriterFactory func(*LogFileInfo) LogEntryWriter

type LogServer struct {
	Fetcher       LogFetcher
	Parser        LogParser
	WriterFactory LogEntryWriterFactory
	Statsd        *statsd.Client
}

type LogEntry struct {
	LogFileInfo
	LogLineEntry
	Environment string `json:"env"`
	ServiceId   string `json:"serviceId"`
	SourceType  string `json:"sourcetype"`
}

type LogFileInfo struct {
	LoadBalancer LogEntryLoadBalancer `json:"load_balancer"`
	LogBucket    string               `json:"log_bucket"`
	LogFilename  string               `json:"log_filename"`
}

type LogEntryLoadBalancer struct {
	AccountId string `json:"aws_account_id"`
	Name      string `json:"name"`
	// TODO: can possibly compress type since only two values
	Type   string            `json:"type"`
	Ip     string            `json:"ip"`
	Region string            `json:"region"`
	Tags   map[string]string `json:"tags,omitempty"`
}

type IpString struct {
	Raw  string `json:"raw,omitempty"`
	Ip   string `json:"ip,omitempty"`
	Port string `json:"port,omitempty"`
}

type LogLineEntry struct {
	ConnectionType         string              `json:"connection_type,omitempty"`
	Timestamp              string              `json:"time"`
	Client                 string              `json:"src_ip"`
	Target                 string              `json:"dest_ip"`
	RequestProcessingTime  float64             `json:"request_processing_time"`
	TargetProcessingTime   float64             `json:"target_processing_time"`
	ResponseProcessingTime float64             `json:"response_processing_time"`
	Duration               float64             `json:"duration,omitempty"`
	LoadBalancerStatusCode string              `json:"status,omitempty"`
	TargetStatusCode       string              `json:"target_status_code,omitempty"`
	Bytes                  int64               `json:"bytes,omitempty"`
	ReceivedBytes          int64               `json:"bytes_in"`
	SentBytes              int64               `json:"bytes_out"`
	Request                LogLineEntryRequest `json:"request"`
	UserAgent              string              `json:"http_user_agent"`
	Port                   int64               `json:"port,omitempty"`
	TlsCipher              string              `json:"tls_cipher,omitempty"`
	TlsProtocol            string              `json:"tls_protocol,omitempty"`
	TargetGroupArn         string              `json:"target_group_arn,omitempty"`
	TraceId                string              `json:"request_id,omitempty"`
}

type LogLineEntryRequest struct {
	Client      string `json:"client"`
	Method      string `json:"http_method,omitempty"`
	Url         string `json:"url,omitempty"`
	HttpVersion string `json:"http_version,omitempty"`
}

func (s *LogServer) Run(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}
		s.handleNextMessages(ctx)
	}
}

func (s *LogServer) handleNextMessages(ctx context.Context) {
	logOpeners, timeout, err := s.Fetcher.GetLogOpeners(ctx)
	if err != nil {
		log.Errorf("error handling received messages: %v", err)
		return
	}

	subcontext, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	wg := sync.WaitGroup{}
	wg.Add(len(logOpeners))
	for _, logOpener := range logOpeners {
		go func(logOpener *LogOpener) {
			defer wg.Done()
			timeStart := time.Now()
			if !s.handleLogOpener(subcontext, logOpener) {
				s.Statsd.Incr(messageProcessingFailedMetric, nil, 1)
			}
			s.Statsd.Timing(messageProcessingTimeMetric, time.Since(timeStart), nil, 1)
		}(logOpener)
	}
	wg.Wait()
}

func (s *LogServer) handleLogOpener(ctx context.Context, logOpener *LogOpener) bool {
	for idx, o := range logOpener.Openers {
		s.Statsd.Count(logfileSizeMetric, o.size, nil, 1)

		startTime := time.Now()
		err := s.handleNextFile(ctx, o)
		duration := time.Since(startTime)
		logFields := log.Fields{
			"bucket":           o.bucket,
			"filename":         o.filename,
			"size":             o.size,
			"duration":         duration,
			"index_in_message": idx,
			"files_in_message": len(logOpener.Openers),
		}
		s.Statsd.Timing(logfileProcessingTimeMetric, duration, nil, 1)

		if err != nil {
			select {
			case <-ctx.Done():
			default:
				log.WithFields(logFields).Errorf("error handling file: %v", err)
			}
			return false
		}
		log.WithFields(logFields).Infof("successfully handled log file")
	}
	if err := logOpener.Success(ctx); err != nil {
		if err != context.Canceled && err != context.DeadlineExceeded {
			log.WithFields(log.Fields{
				"logOpener": fmt.Sprintf("%+v", logOpener),
			}).Errorf("error handling file: %v", err)
		}
		return false
	}
	return true
}

func (s *LogServer) handleNextFile(ctx context.Context, opener S3LogFileOpener) error {
	var logFileInfo LogFileInfo
	rawReader, err := opener.Open(ctx, &logFileInfo)
	if err != nil {
		return ContextOrError(ctx, "error opening file: %v", err)
	}
	defer rawReader.Close()

	logData := bytes.Buffer{}
	_, err = logData.ReadFrom(rawReader)
	if err != nil {
		return ContextOrError(ctx, "error reading file into memory: %v", err)
	}

	// note on memory use:
	// the largest access log file we have seen is 7,356,631 bytes, as per:
	// `aws s3api list-objects-v2 --bucket my-logs | jq '.Contents | map(.Size) | sort | .[-1]'`
	// so there will be multiple whole copies of each log file in memory :(

	// TODO: investigate creating a custom Scanner / Reader / Transformer,
	// to do this in a more memory-efficient manner

	redacted := redact.RedactJwts(logData.String())

	// TODO: write the redacted version back to the log bucket

	// TODO: if an S3 Object was (re)written,
	// then this will trigger a new event,
	// in which case we should stop processing this file,
	// and assume we we process the redacted version in a future invocation

	parseResults := s.Parser.ParseReader(ctx, strings.NewReader(redacted), &logFileInfo)

	writer := s.WriterFactory(&logFileInfo)
	for sink := range parseResults {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case result, ok := <-sink:
			if !ok {
				// Unable to parse the line.
				continue
			}
			if result.err != nil {
				// Detected an error while reading: return and log this error, and leave this logfile on the queue
				// for retrying.
				return result.err
			}
			err := writer.PushEntry(ctx, result.logLineEntry)
			if err != nil {
				return err
			}
		}
	}
	return writer.Flush(ctx)
}
