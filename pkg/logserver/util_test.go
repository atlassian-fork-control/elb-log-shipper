package logserver

import (
	"regexp"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindNamedStringSubmatches(t *testing.T) {
	t.Parallel()

	r := regexp.MustCompile(`^(?P<alpha>[a-z]+)(?P<under>_)?(?P<num>\d+)$`)
	submatchTests := []struct {
		name     string
		regex    *regexp.Regexp
		s        string
		expected map[string]string
	}{
		{"All tokens present", r, "abc_123", map[string]string{
			"":      "abc_123",
			"alpha": "abc",
			"under": "_",
			"num":   "123",
		}},
		{"Optional group empty", r, "a1", map[string]string{
			"":      "a1",
			"alpha": "a",
			"under": "",
			"num":   "1",
		}},
		{"No match", r, "!@#", nil},
	}
	for pos, test := range submatchTests {
		test := test
		t.Run(strconv.Itoa(pos), func(t *testing.T) {
			t.Parallel()
			assert.Equal(t, test.expected, findNamedStringSubmatches(test.regex, test.s))
		})
	}
}
