package redact

import (
	"encoding/base64"
	"encoding/json"
	"regexp"
	"strings"
)

const (
	// maxJwtSize         = 8096 // https://stackoverflow.com/a/30781322/488373
	defaultReplacement = "redacted"
)

var (
	// JSON:"{}" is Base64URL:"e30", so JSON parts must be 3 characters or more
	// JWTs use Base64URL encoding, _not_ Base64
	// JWS / JWTs omit the trailing '=' from Base64
	jwtRegExp = regexp.MustCompile("\\b[\\w-]{3,}\\.[\\w-]{3,}\\.[\\w-]+\\b")
)

func RedactJwts(s string) string {
	return jwtRegExp.ReplaceAllStringFunc(s, func(match string) string {
		if isJwt(match) {
			return defaultReplacement
		}
		return match
	})
}

// Base64 uses "+" and "/": https://tools.ietf.org/html/rfc4648#section-4
// Base64URL uses "-" and "_": https://tools.ietf.org/html/rfc4648#section-5
func decodeBase64Url(s string) ([]byte, error) {
	// convert Base64URL to Base64 first, then decode
	return base64.StdEncoding.DecodeString(strings.Map(func(r rune) rune {
		if r == '-' {
			return '+'
		}
		if r == '_' {
			return '/'
		}
		return r
	}, s))
}

func isJsonObject(data []byte) bool {
	var result map[string]interface{}
	return json.Unmarshal(data, &result) == nil
}

func isJwt(s string) bool {
	token := strings.Split(s, ".") // []string{"header", "payload", "signature"}
	if len(token) != 3 {
		return false
	}
	decodedHeader, err := decodeBase64Url(token[0])
	if err != nil {
		return false
	}
	decodedPayload, err := decodeBase64Url(token[1])
	if err != nil {
		return false
	}
	return isJsonObject(decodedHeader) && isJsonObject(decodedPayload)
}
